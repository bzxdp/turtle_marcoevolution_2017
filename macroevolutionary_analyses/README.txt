THIS FOLDER CONTAINS CODE TO REPEAT THE ANALYSES FROM RPANDA AS WELL AS THE FOSSIL DIVERSITY DOWNLOADED FROM THE PALEOBIOLOGY DATABASES AND THE TEMPERATURE DATA FROM CARDENAS AND HARRIES 2010. 

Four trees used in the analyses:
	cir_cal1.chronogram
	cir_cal2.chronogram
	ugamma_cal1.chronogram
	ugamma_cal2.chronogram

Fossil diversity data downloaded from the Paleobiology database:
	sqs_0.8_stageBin.csv

Temperature data from Cardenas and Harries 2010:
	CardenasHarries_2010.csv

R code to repeat the RPANDA analyses:
	rPanda.R
	estimateDiversity.R